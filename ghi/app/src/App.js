import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Nav from './Nav';
import MainPage from './MainPage';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeesForm from './AttendeesForm';
import PresentationForm from './PresentationForm';
import AttendeesList from './AttendeesList';



function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <>
      <BrowserRouter>
        <Nav />
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="presentations/new" element={<PresentationForm />} />
            <Route path="attendees/new" element={<AttendeesForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
        </BrowserRouter>
      </>
  );
}

export default App;
