import React, { useEffect, useState } from 'react';

function ConferenceForm (props) {

    //CHANGE INPUTS WITH USER'S ENTRIES------------------------------------------------------
    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setPresentations] = useState('');
    const [maxAttendees, setAttendees] = useState('');
    const [selectedLocation, setSelectedLocation] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        return setStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        return setEnd(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        return setDescription(value);
    }
    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        return setPresentations(value);
    }
    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        return setAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        return setSelectedLocation(value);
    }

    //GETS LIST OF LOCATIONS INSIDE THE DATA VARIABLE---------------------------------------------

    const [locations, setLocations] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            //ADD LIST OF LOCATIONS FOR THE SELECT BUTTON-----------------------------------------
            setLocations(data.locations)
        }
    }

    // useEffect() is a hook to make fectch requests to get data:
    useEffect(() => {
        fetchData();
    }, []);

    //SAVE FORM WHEN CLICKED ON "Ceate" BUTTON---------------------------------------------------
    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = selectedLocation;

        console.log(data);

        //Sends data to the server:
        const conferenceUrl = 'http://localhost:8000/api/conferences/';

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setSelectedLocation('');
        }
    }

    //RETURNS THE FORM FOR A NEW CONFERENCE--------------------------------------------------------
    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={start} onChange={handleStartChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={end} onChange={handleEndChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea value={description} onChange={handleDescriptionChange} className="form-control" placeholder="Description" required type="text" name="description" id="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxPresentations} onChange={handlePresentationsChange} placeholder="MaxiumumPresentations" required type="text" name="max_presentations" id="max_presentations" className="form-control"/>
                            <label htmlFor="max_presentations">Maxiumum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxAttendees} onChange={handleAttendeesChange} placeholder="MaxiumumAttendees" required type="text" name="max_attendees" id="max_attendees" className="form-control"/>
                            <label htmlFor="max_attendees">Maxiumum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={selectedLocation} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                {locations.map(location => {
                                    return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                    );
                                })}
                            </select>
                        </div>
                            <button className="btn btn-primary">Create</button>
                    </form>
                    </div>
                </div>
            </div>
      </div>
    );
}

export default ConferenceForm;
