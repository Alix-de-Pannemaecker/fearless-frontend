import React, { useEffect, useState } from 'react';

function LocationForm (props) {

    //CHANGE INPUTS WITH USER'S ENTRIES------------------------------------------------------
    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [selectedState, setSelectedStates] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        return setRoomCount(value);
    }
    const handleCityChange = (event) => {
        const value = event.target.value;
        return setCity(value);
    }
    const handleStateChange = (event) => {
        const value = event.target.value;
        return setSelectedStates(value);
    }

    //GETS LIST OF STATES INSIDE THE DATA VARIABLE---------------------------------------------

    const [states, setStates] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            //ADD LIST OF STATES FOR THE SELECT BUTTON-----------------------------------------
            setStates(data.states)
        }
    }

    // useEffect() is a hook to make fectch requests to get data:
    useEffect(() => {
        fetchData();
    }, []);

    //SAVE FORM WHEN CLICKED ON "Ceate" BUTTON---------------------------------------------------
    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = selectedState;

        console.log(data);

        //Sends data to the server:
        const locationUrl = 'http://localhost:8000/api/locations/';

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setSelectedStates('');
        }
    }

    //RETURNS THE FORM FOR A NEW LOCATION--------------------------------------------------------
    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input value={name} name="name" onChange={handleNameChange} placeholder="Name" required type="text" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={roomCount} name="room_count" onChange={handleRoomCountChange} placeholder="Room count" required type="number" id="room_count" className="form-control"/>
                        <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={city} name="city" onChange={handleCityChange} placeholder="City" required type="text" id="city" className="form-control"/>
                        <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                        <select value={selectedState} onChange={handleStateChange} required name="state" id="state" className="form-select">
                        <option value="">Choose a state</option>
                        {states.map(state => {
                            return (
                            <option key={state.abbreviation} value={state.abbreviation}>
                                {state.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    );
}

export default LocationForm;
